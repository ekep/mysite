from django.conf.urls import url
from Site.views import home, items, dropshipers, orders, ordersdrop, droper

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^items$', items, name='items'),
    url(r'^dropshipers$', dropshipers, name='dropshipers'),
    url(r'^orders$', orders, name='orders'),
    url(r'^ordersdrop$', ordersdrop, name='ordersdrop'),
    url(r'^(?P<alias>[^/]+)$', droper, name='droper'),
]
