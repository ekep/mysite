from django.shortcuts import render_to_response
from Site.models import Item, Order, Shop, OrderItem


def home(request):
    drop = Order.objects.filter(shop=True)
    args = {}
    args['title'] = 'Головна'
    args['countitem'] = Item.objects.count()
    args['countshop'] = Shop.objects.count()
    args['countorder'] = Order.objects.count()
    args['countdrop'] = len(drop)
    return render_to_response('home.html', args)


def dropshipers(request):
    args = {}
    args['title'] = 'Дропшиппери'
    args['shops'] = Shop.objects.all()

    return render_to_response('dropshipers.html', args)


def items(request):
    args = {}
    args['title'] = 'Товари'
    args['items'] = Item.objects.all()
    return render_to_response('items.html', args)


def orders(request):
    args = {}
    args['title'] = 'Заказ'
    args['orders'] = [o.s_dict for o in Order.objects.all()]
    return render_to_response('orders.html', args)


def ordersdrop(request):
    args = {}
    args['title'] = 'Закази дропшиперів'
    # args['ordersdrop'] = Order.objects.filter(shop=True)
    args['orders'] = [o.s_dict for o in Order.objects.filter(shop=True)]
    return render_to_response('ordersdrop.html', args)


def droper(request):
    args = {}
    args['title'] = 'Закази'
    args['orders'] = [o.s_dict for o in Order.objects.filter(shop=shop)]
    return render_to_response('droper.html', args)
