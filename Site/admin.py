from django.contrib import admin
from Site.models import Item, Order, OrderItem, Shop, Pay


class ItemAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'buyprice', 'dropprice', 'saleprice')
    list_display_links = ('code', 'name')
    list_per_page = 25
    prepopulated_fields = {"alias": ('code',)}


class OrderItemInline(admin.StackedInline):
    model = OrderItem
    extra = 1


class OrderAdmin(admin.ModelAdmin):
    list_display = ('date', 'client', 'shop', 'total', 'payment', 'deliver')
    inlines = [OrderItemInline]


class ShopAdmin(admin.ModelAdmin):
    list_display = ('shop', 'mail')
    list_display_links = ('shop', 'mail')
    list_per_page = 10
    prepopulated_fields = {"alias": ('shop',)}


class PayAdmin(admin.ModelAdmin):
    list_display = ('type', 'shop', 'ord', 'price')


admin.site.register(Item, ItemAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(Pay, PayAdmin)
