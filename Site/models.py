import decimal
from django.db import models


class Item(models.Model):
    code = models.CharField(max_length=255, verbose_name='Код товара')
    name = models.CharField(max_length=255, verbose_name='Назва товара')
    buyprice = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна закупки')
    dropprice = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна дропшиппинг')
    saleprice = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна продажі')
    alias = models.SlugField(verbose_name='Alias товара')

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товари"

    def __str__(self):
        return self.name


class Shop(models.Model):
    shop = models.CharField(max_length=255, verbose_name='Назва магазину')
    mail = models.EmailField(verbose_name='Мейл магазину')
    alias = models.SlugField(verbose_name='Alias магазину')

    class Meta:
        verbose_name = "Магазин"
        verbose_name_plural = "Магазини"

    def __str__(self):
        return self.shop


class Order(models.Model):
    AFTPAY = 1
    PREPAY = 2
    NEW = 1
    ACCEPTED = 2
    CLOSED = 3
    DECLINED = 4

    PAYMENT_STATUSES = ((AFTPAY, 'Наложений платіж'), (PREPAY, 'Предоплата'))
    DELIVER_STATUSES = ((NEW, 'Новий'), (ACCEPTED, 'Прийнято'), (CLOSED, 'Виконано'), (DECLINED, 'Відмінено'))
    date = models.DateTimeField(verbose_name='Дата Замовлення')
    client = models.CharField(max_length=255, verbose_name='Клієнт')
    payment = models.IntegerField(choices=PAYMENT_STATUSES, verbose_name='Тип оплати')
    shop = models.ForeignKey(Shop, blank=True, null=True, verbose_name='Магазин')
    deliver = models.IntegerField(choices=DELIVER_STATUSES, verbose_name='Статус')

    class Meta:
        verbose_name = "Замовлення"
        verbose_name_plural = "Замовлення"

    def total(self):
        ttal = decimal.Decimal('0.00')
        order_items = OrderItem.objects.filter(order=self)
        for item in order_items:
            ttal += item.total()
        return ttal

    def s_dict(self):
        itm = OrderItem.objects.filter(order_id=self.id)
        items = [i.item.name for i in itm]
        return dict(
            id=self.id,
            total=self.total(),
            date=self.date,
            client=self.client,
            shop=self.shop,
            payment=self.get_payment_display,
            deliver=self.get_deliver_display,
            items=items
        )

    def __str__(self):
        return self.client


class OrderItem(models.Model):
    order = models.ForeignKey(Order, verbose_name='Замовлення')
    item = models.ForeignKey(Item, verbose_name='Товар')
    quantity = models.IntegerField(default=1, verbose_name='Кількість')
    buyprice = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна закупки')
    dropprice = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True, verbose_name='Ціна дропшиппинг')
    saleprice = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна продажі')

    def total(self):
        return self.quantity * self.saleprice


class Pay(models.Model):
    PAY = 1
    AFPAY = 2
    PRPAY = 3

    PAY_STATUSES = ((PAY, 'Виплата'), (AFPAY, 'Одержано наложений платіж'), (PRPAY, 'Оплачено'))
    date = models.DateTimeField(verbose_name='Дата оплати')
    type = models.IntegerField(choices=PAY_STATUSES, verbose_name='Статус')
    shop = models.ForeignKey(Shop)
    price = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Сумма')
    ord = models.ForeignKey(Order, blank=True, null=True,)

    class Meta:
        verbose_name = "Платіж"
        verbose_name_plural = "Платежі"
